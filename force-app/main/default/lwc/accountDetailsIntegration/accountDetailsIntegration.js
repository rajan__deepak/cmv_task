/* eslint-disable no-case-declarations */
/* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Summary :       This is the JS controller for component.
 * ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Deepak Rajan <rajan_deepak@outlook.com>
 * @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
 * @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
 * @version        1.0
 * @createdDate    23-08-2021
 * @modified
 * @systemLayer    JS
 * @see            ????
 * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * @changes
 * @modifiedBy
 * @modifiedDate
 * @version          1.1
 * @methodName
 *
 * ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
import { LightningElement } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import fetchAccountRecords from "@salesforce/apex/CMW_AccountIntegration.fetchAccountRecords";
import { COLUMNS, reduceErrors } from "c/utilityHelper";
import ERROR from "@salesforce/label/c.ErrorVariant";
import ERROR_TITLE from "@salesforce/label/c.ErrorTitle";
import TARGET_ORG_URL from "@salesforce/label/c.Org2DevUrl";

export default class AccountDetailsIntegration extends LightningElement {
  /*
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
   * @description      Variables declaration.
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   */
  //to store the search string
  searchText = "";
  //to store the accounts retreived from other org
  accounts;
  // to store the columns used in lightning-datatable
  columns = COLUMNS;
  //to show/hide the spinner in UI
  showSpinner = false;
  // to store the custom label values alltogether
  labels = {
    ERROR,
    ERROR_TITLE,
    TARGET_ORG_URL
  };

  /*
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
   * @description      Used to get input value for searched Account Name.
   * @parameter        event
   * @returnValue
   * @example
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   */
  handleInputChange(event) {
    //storing the input text into variable
    this.searchText = event.target.value;
  }
  /*
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
   * @description      Used to get accounts data from SF.
   * @parameter        event
   * @returnValue
   * @example
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   */
  handleSearch() {
    //checking if seartext is present and has length more than 2
    if (this.searchText && this.searchText.length > 2) {
      this.showSpinner = true;
      //making a call to server to pull accounts
      fetchAccountRecords({ searchValue: this.searchText })
        .then((results) => {
          let output = JSON.parse(results);
          //checking if there is an error while making an callout from server
          if (output.errorMessage) {
            this.showSpinner = false;
            this.fireToast(
              this.labels.ERROR_TITLE,
              output.errorMessage,
              this.labels.ERROR
            );
          }
          //processing the records which are successfully retrieved
          else {
            let flag =
              output.result &&
              JSON.parse(output.result).searchRecords.length > 0;
            switch (flag) {
              case true:
                //formatting and creating an array of accounts records
                let records = this.processRecords(JSON.parse(output.result));
                this.accounts = records;
                this.showSpinner = false;
                break;
              case false:
                // if zero accounts are found for searched string, display an error
                this.fireToast(
                  this.labels.ERROR_TITLE,
                  "No record found. Please enhance the search",
                  this.labels.ERROR
                );
                //reset the values
                this.accounts = {};
                this.showSpinner = false;
                break;
              default:
            }
          }
        })
        .catch((error) => {
          this.fireToast(
            this.labels.ERROR_TITLE,
            reduceErrors(error).join(", "),
            this.labels.ERROR
          );
        });
    } else {
      // display an error if input text length is less than 2 chatracters
      this.fireToast(
        this.labels.ERROR_TITLE,
        "Please enter a search String of more than 2 characters",
        this.labels.ERROR
      );
      //reset the value
      this.accounts = {};
    }
  }

  /*
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
   * @description      This is method for creating a new object of Account Records.
   * @parameter        inputRecords
   * @returnValue
   * @example
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   */
  processRecords(inputRecords) {
    //create a new array of received response from server
    let recordsProcessed = inputRecords.searchRecords.map((val) => ({
      Id: val.Id,
      linkName: this.labels.TARGET_ORG_URL + "/" + val.Id,
      Name: val.Name,
      BillingCountry: val.BillingCountry,
      Number_of_open_opportunities__c: val.Number_of_open_opportunities__c,
      Number_of_contacts__c: val.Number_of_contacts__c
        ? val.Number_of_contacts__c
        : 0
    }));
    return recordsProcessed;
  }

  /*
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
   * @description      This is method for firing toast message for all variant types.
   * @parameter        _title,_message,_variant
   * @returnValue
   * @example
   * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
   */
  fireToast(_title, _message, _variant) {
    const toast = new ShowToastEvent({
      title: _title,
      message: _message,
      variant: _variant
    });
    this.dispatchEvent(toast);
  }
}
