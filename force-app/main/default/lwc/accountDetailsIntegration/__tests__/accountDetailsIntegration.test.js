/* eslint-disable jest/no-commented-out-tests */
import { createElement } from "lwc";
import AccountDetailsIntegration from "c/accountDetailsIntegration";
import { registerApexTestWireAdapter } from "@salesforce/sfdx-lwc-jest";
import fetchAccountRecords from "@salesforce/apex/CMW_AccountIntegration.fetchAccountRecords";

//represent a list of valid records
const mockAccountDataList = require("./data/getRecord.json");
//represent a list of no records
const mockNoAccountData = require("./data/getRecordNoData.json");
//register the Apex wire adapter
const getAccountListAdapter = registerApexTestWireAdapter(fetchAccountRecords);

describe("c-wire-apex", () => {
  afterEach(() => {
    //since DOM instance is shared across test cases in a single file hence reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
    jest.clearAllMocks();
  });

  describe("search accounts with search parameter", () => {
    it("called with data from input", () => {
      const USER_INPUT = "United";
      //create the accountDetailsIntegration element
      const element = createElement("c-account-details-integration", {
        is: AccountDetailsIntegration
      });
      document.body.appendChild(element);

      // simulate the user input
      const inputValue = element.shadowRoot.querySelector("lightning-input");
      inputValue.dispatchEvent(
        new CustomEvent("change", { detail: { value: USER_INPUT } })
      );
      // simulate the button click
      const button = element.shadowRoot.querySelector("lightning-button");
      button.click();
      getAccountListAdapter.emit(mockAccountDataList);

      //return a promise to wait for async results
      return Promise.resolve().then(() => {
        const detailElements = element.shadowRoot.querySelector(
          "lightning-datatable"
        );

        expect(detailElements).not.toBeNull();
        expect(detailElements.length).toBe(getAccountListAdapter.length);
      });
    });

    it("called with data is not available", () => {
      const USER_INPUT = "";
      //create the accountDetailsIntegration element
      const element = createElement("c-account-details-integration", {
        is: AccountDetailsIntegration
      });
      document.body.appendChild(element);

      // simulate the user input
      const inputValue = element.shadowRoot.querySelector("lightning-input");
      inputValue.value = USER_INPUT;
      inputValue.dispatchEvent(new CustomEvent("change"));
      getAccountListAdapter.emit(mockNoAccountData);

      //return a promise to wait for async results
      return Promise.resolve().then(() => {
        const detailElements = element.shadowRoot.querySelector(
          "lightning-datatable"
        );
        expect(detailElements.length).toBe(getAccountListAdapter.length);
      });
    });
  });
});
