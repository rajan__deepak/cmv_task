/*
 * ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Summary :       This is the js util class.
 * ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Deepak Rajan <rajan_deepak@outlook.com>
 * @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
 * @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
 * @version        1.0
 * @createdDate    24-08-2021
 * @modified
 * @systemLayer    JS
 * @see            ????
 * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * @changes
 * @modifiedBy
 * @modifiedDate
 * @version          1.1
 * @methodName
 *
 * ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

//creating the columns for accountDetailsIntegration component
const COLUMNS = [
  {
    label: "Account Name",
    fieldName: "linkName",
    type: "url",
    typeAttributes: {
      label: { fieldName: "Name" },
      value: { fieldName: "linkName" },
      target: "_blank"
    }
  },
  {
    label: "Billing Country",
    fieldName: "BillingCountry",
    type: "text",
    cellAttributes: { alignment: "left" }
  },
  {
    label: "Number of Opportunties",
    fieldName: "Number_of_open_opportunities__c",
    type: "number",
    cellAttributes: { alignment: "left" }
  },
  {
    label: "Number of Contacts",
    fieldName: "Number_of_contacts__c",
    type: "number",
    cellAttributes: { alignment: "left" }
  }
];
export { COLUMNS };

export function reduceErrors(errors) {
  if (!Array.isArray(errors)) {
    errors = [errors];
  }

  return (
    errors
      // Remove null/undefined items
      .filter((error) => !!error)
      // Extract an error message
      .map((error) => {
        // UI API read errors
        if (Array.isArray(error.body)) {
          return error.body.map((e) => e.message);
        }
        // UI API DML, Apex and network errors
        else if (error.body && typeof error.body.message === "string") {
          return error.body.message;
        }
        // JS errors
        else if (typeof error.message === "string") {
          return error.message;
        }
        // Unknown error shape so try HTTP status text
        return error.statusText;
      })
      // Flatten
      .reduce((prev, curr) => prev.concat(curr), [])
      // Remove empty strings
      .filter((message) => !!message)
  );
}
