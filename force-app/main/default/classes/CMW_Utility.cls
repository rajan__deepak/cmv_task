/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* Summary :       This is the Utility class for Commvault.
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Deepak Rajan <rajan_deepak@outlook.com>
* @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
* @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
* @name           CMW_Utility
* @version        1.0 
* @createdDate    23-08-2021
* @modified       
* @systemLayer    Apex Class  
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedBy       
* @modifiedDate     
* @version          1.1
* @methodName       
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class CMW_Utility {
    
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    * @description      Used to create create an HTTPRequest.
    * @parameter        endPoint, methodType
    * @returnValue      HTTPRequest
    * @example          CMW_Utility.createHTTPRequest('https://login.salesforce.com','POST');
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    */    
    public static HTTPRequest createHTTPRequest(String endPoint, String methodType) {
        //create an request and set the parameters
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endPoint);
        request.setMethod(methodType);
        return request;        
    }
    
        
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    * @description      Used to get response of an HTTPRequest.
    * @parameter        request
    * @returnValue      HTTPResponse
    * @example          CMW_Utility.getHTTPResponse(request)
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    */    
    public static HTTPResponse getHTTPResponse(HTTPRequest request) {
        //get an HTTPResponse
        Http http = new Http();
        HTTPResponse response = http.send(request);
        return response;
    }
}