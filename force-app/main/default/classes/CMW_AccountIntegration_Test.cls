/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* Summary :       This is test class for CMW_AccountIntegration.
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Deepak Rajan <rajan_deepak@outlook.com> 
* @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
* @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
* @name           CMW_AccountIntegration_Test
* @version        1.0
* @createdDate    24-08-2021
* @modified       
* @systemLayer    Apex Class
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedBy       
* @modifiedDate     
* @version          1.1
* @methodName       
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class CMW_AccountIntegration_Test {
    
    //set up the test data
    @TestSetup
    static void createTestData() {}
    
    //method to check success case from callout
    static testMethod void checkSuccessCase() {        
        Test.startTest();
        //set the mock data
        Test.setMock(HttpCalloutMock.class, new CMW_AccountIntegrationMockService());
        //call the method
        String results = CMW_AccountIntegration.fetchAccountRecords('United'); 
        //deserialize the response         
        CMW_AccountIntegration.ResponseWrapper wrapper = (CMW_AccountIntegration.ResponseWrapper)JSON.deserialize(results, CMW_AccountIntegration.ResponseWrapper.Class);
       
        //assesrtion statments
        System.assert(String.isBlank(wrapper.errorMessage)); 
        System.assert(String.isNotBlank(wrapper.result)); 
        Test.stopTest();        
    }
    
    //method to check faliure case from callout
    static testMethod void checkFaliureCase() {        
        Test.startTest();
        //set the mock data
        Test.setMock(HttpCalloutMock.class, new CMW_AccountIntegrationFaliureMockService());
        //call the method
        String results = CMW_AccountIntegration.fetchAccountRecords('United');  
        //deserialize the response 
        CMW_AccountIntegration.ResponseWrapper wrapper = (CMW_AccountIntegration.ResponseWrapper)JSON.deserialize(results, CMW_AccountIntegration.ResponseWrapper.Class);
        
        //assesrtion statments
        System.assert(String.isBlank(wrapper.errorMessage)); 
        System.assertEquals(400, wrapper.statusCode);
        Test.stopTest();        
    }
    
}