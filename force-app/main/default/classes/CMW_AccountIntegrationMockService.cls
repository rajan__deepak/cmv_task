/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* Summary :       This is the test mock class for CMW_AccountIntegration.
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Deepak Rajan <rajan_deepak@outlook.com>
* @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
* @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
* @name           CMW_AccountIntegrationMockService
* @version        1.0
* @createdDate    24-08-2021
* @modified       
* @systemLayer    Apex Class
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedBy       
* @modifiedDate     
* @version          1.1
* @methodName       
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class CMW_AccountIntegrationMockService implements HttpCalloutMock {
    //mock response method
    public HTTPResponse respond(HTTPRequest req) 
    {
        HttpResponse resp = new HttpResponse();
        String response = '{"searchRecords" : [ { "attributes" : { "type" : "Account","url" : "/services/data/v51.0/sobjects/Account/0015g00000P5FOSAA3"},"Id" : "0015g00000P5FOSAA3", "Name" : "United Oil & Gas Corp.",    "Number_of_open_opportunities__c" : 4.0,"Number_of_contacts__c" : null, "BillingCountry" : "USA" }]}';
        resp.setStatusCode(200);
        resp.setBody(response);
        return resp;
    }
}
