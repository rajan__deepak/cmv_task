/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* Summary :       This is the test mock class for CMW_AccountIntegration.
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Deepak Rajan <rajan_deepak@outlook.com>
* @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
* @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
* @name           CMW_AccountIntegrationFaliureMockService
* @version        1.0
* @createdDate    25-08-2021
* @modified       
* @systemLayer    Apex Class
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedBy       
* @modifiedDate     
* @version          1.1
* @methodName       
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class CMW_AccountIntegrationFaliureMockService implements HttpCalloutMock {
    //mock response method
    public HTTPResponse respond(HTTPRequest req) 
    {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(400);
        resp.setBody('[]');
        return resp;
    }
}
