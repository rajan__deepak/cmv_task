/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* Summary :       This is the Service class for fetching account records from another org.
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Deepak Rajan <rajan_deepak@outlook.com>
* @createdBy      Deepak Rajan <rajan_deepak@outlook.com>
* @maintainedBy   Deepak Rajan <rajan_deepak@outlook.com>
* @name           CMW_AccountIntegration
* @version        1.0
* @createdDate    23-08-2021
* @modified       
* @systemLayer    Apex Class
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* @modifiedBy       
* @modifiedDate     
* @version          1.1
* @methodName       
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class CMW_AccountIntegration {
    
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    * @description      Used to fetch Account Records from other org based on a string.
    * @parameter        searchValue
    * @returnValue      String
    * @example          CMW_AccountIntegration.fetchAccountRecords('United');
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    */    
    @AuraEnabled
    public static String fetchAccountRecords(String searchValue) {
        //response wrapper instance
        ResponseWrapper wrapper = new ResponseWrapper();
        
        //Endcoding searchValue to prevent loss of string characters//
        //preventing the soql injection using String.escapeSingleQuotes
        String accName = EncodingUtil.urlEncode(String.escapeSingleQuotes(searchValue), 'UTF-8');
        
        //setting the endpoint using named credentials and REST API using parameterizedSearch
        //using REST API to query fields and using limit to restrcit number of records pulled
        String endPoint = 'callout:Integration_with_Commvault/services/data/v51.0/parameterizedSearch/?q='+'\''+accName+'\'&sobject=Account&Account.fields=id,name,Number_of_open_opportunities__c,Number_of_contacts__c,BillingCountry&Account.limit=50';
       
        //creating a HTTPRequest
        HttpRequest request = CMW_Utility.createHTTPRequest(endPoint, 'GET');
        
        //getting HTTPResponse for the request send
        // if success response is returned, the response body will be stored in ResponseWrapper instance
        //if error response is returned, the status code is being populated and hence used in UI to trigger notification.
        try {
             HTTPResponse resp = CMW_Utility.getHTTPResponse(request);
             if(resp.getStatusCode() == 200 ){
                  wrapper.result = resp.getBody();
             }
             else {
                  wrapper.StatusCode = resp.getStatusCode();
             }
        }       
        catch(Exception ex) {
                   wrapper.errorMessage  = String.valueOf(ex.getCause());
        }
        // result format is String and the parsing is being done into js file on web component
        return JSON.serialize(wrapper);
    }

    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    * @description      Wrapper used to send response to UI.
    * ──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public class ResponseWrapper {
        @AuraEnabled
        public String result {get;set;}
        @AuraEnabled
        public String errorMessage {get;set;}
        @AuraEnabled
        public Integer statusCode {get;set;}
    }
}